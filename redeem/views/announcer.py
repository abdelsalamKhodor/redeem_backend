'''
Created on Aug 24, 2016

@author: User
'''
from django.shortcuts import render_to_response, redirect
from redeem.common import tools
from django.conf.urls import url
from django.core.urlresolvers import reverse
from redeem.models.relational import Announcer, Client
import logging
from redeem.forms import SignleDocumentForm
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from redeem.common.tools import get_hash_of_string
from django.contrib.auth.models import User, Group
from redeem.common.constants import group
import os
from redeem import settings

log = logging.getLogger(__name__)
def announcers_grid(request):
    try:
        ctx = tools.defaultContextData(request)
        return render_to_response('announcer/announcer_grid.html',ctx)
    except Exception as e:
        log.error("An error occurred while get announcers grid page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def get_announcers(request):
    try:
        announcersQset = Announcer.objects.all()
        rows = []
        for obj in announcersQset:
            rows.append({'id' : obj.pk,'name' : obj.name, 'phone':obj.phone, 'email':obj.email})
        return JsonResponse({'rows' : rows})
    except Exception as e:
        log.error("An error occurred while get announcers info : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def announcer_form(request):
    try:
        announcer_id = request.GET.get('id')
        announcer = Announcer.objects.filter(id = announcer_id).first()
        ctx = tools.defaultContextData(request)
        ctx['announcer'] = announcer
        ctx['form'] = SignleDocumentForm()
        ctx['MEDIA_URL'] = settings.MEDIA_URL
        return render_to_response('announcer/announcer_form.html', ctx)
    except Exception as e:
        log.error("An error occurred while get announcer form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

@csrf_exempt
def save_announcer_form(request):
    try:
        ctx = tools.defaultContextData(request)
        post = request.POST
        announcer_id = post.get('id')
        name = post.get('announcer_name')
        email = post.get('email')
        phone = post.get('phone')
        username = post.get('username')
        password = post.get('password')
        form = SignleDocumentForm(request.POST, request.FILES)
        logo_url = None
        if 'docfile' in request.FILES:
            logo_url = request.FILES.get('docfile')
        
        if announcer_id:
            edit_announcer(announcer_id, name, email, phone, form, logo_url)
        
        elif all([name, email, phone, username, password]):
            user = User()
            user.username = username
            user.email = email
            hashed_password = get_hash_of_string(password)
            user.password = hashed_password
            user.save()
            
            group = Group.objects.get(name='announcer')
            user.groups.add(group)
            user.save()
            
            announcer = Announcer()
            announcer.name = name
            announcer.email = email
            announcer.phone= phone
            announcer.user = user
            
            if form.is_valid() and logo_url:
                announcer.logo_url = logo_url
                fullname = os.path.join(settings.MEDIA_ROOT, announcer.logo_url.name)
                if os.path.exists(fullname):
                    os.remove(fullname)
            announcer.save()
        ctx['active_tab'] = 'announcer'
        return render_to_response("users.html", ctx)
            
    except Exception as e:
        log.error("An error occurred while submit announcer form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def edit_announcer(announcer_id, name, email, phone, form, logo_url):
    if all([announcer_id, name, email, phone]):
        announcer = Announcer.objects.filter(id = announcer_id).first()
        announcer.name = name
        announcer.phone = phone
        announcer.email = email
        user = announcer.user
        user.email = email
        user.save()
        
        if form.is_valid() and logo_url:
            announcer.logo_url = logo_url
            announcer.logo_url.name = os.path.join('photos/announcers',str(announcer.name)+'.png')
            fullname = os.path.join(settings.MEDIA_ROOT, announcer.logo_url.name)
            if os.path.exists(fullname):
                os.remove(fullname)
        announcer.save()

def delete_announcer_form(request):
    try:
        ctx = tools.defaultContextData(request)
        announcer_id = request.GET.get('id')
        ctx['announcer_id'] = announcer_id
        announcer = Announcer.objects.filter(id = announcer_id)
        clients = Client.objects.filter(announcer = announcer )
        if clients:
            ctx['can_delete'] = False
        else:
            ctx['can_delete'] = True   
        return render_to_response('announcer/announcer_delete_form.html',ctx)
    except Exception as e:
        log.error("An error occurred while get delete announcer form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

@csrf_exempt
def delete_announcer(request):
    try:
        announcer_id = request.POST.get('id')
        announcer = Announcer.objects.filter(id = announcer_id).first()
        user = announcer.user
        user.delete()
        announcer.delete()
        return JsonResponse({'status' : 'OK'})
    except Exception as e:
        log.error("An error occurred while delete announcer : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))


urls = [
    url (r'^announcers_grid$', announcers_grid, name="announcers_grid"),
    url (r'^get_announcers$', get_announcers, name="get_announcers"),
    url (r'^announcer_form$', announcer_form, name="announcer_form"),
    url (r'^save_announcer_form$', save_announcer_form, name="save_announcer_form"),
    url (r'^delete_announcer_form$', delete_announcer_form, name="delete_announcer_form"),
    url (r'^delete_announcer$', delete_announcer, name="delete_announcer"),
    ]