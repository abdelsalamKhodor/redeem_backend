'''
Created on Aug 22, 2016

@author: User
'''
from redeem.models.relational import Category, Sub_category, User,\
    User_categorie
from redeem.common.tools import ajax_response, get_hash_of_string
import uuid
import logging
from django.views.decorators.csrf import csrf_exempt
import json

log = logging.getLogger(__name__)
def get_categories(request):
    status = 'OK'
    message = ''
    data = []
    try:
        categories = Category.objects.all()
        for category in categories:
            count = Sub_category.objects.filter(category_id = category.pk).count()
            data.append({'id' : category.pk ,'name' : category.name, 'image' : category.cat_image.url, 'count' : count})
    except Exception as e:
        status = 'Error'
        message = e.message
    return ajax_response({'status':status, 'message':message, 'data':data})

def get_sub_categories(request):
    status = 'OK'
    message = ''
    data = []
    try:
        cat_id = request.GET['categoryId']
        if cat_id:
            data = []
            subs = Sub_category.objects.filter(category = cat_id)
            for sub in subs:
                data.append({'id' : sub.pk, 'name': sub.name, 'image' : sub.sub_cat_image})
    except Exception as e:
        status = 'Error'
        message = e.message
    return ajax_response({'status':status, 'message':message, 'data':data})

@csrf_exempt
def register_user(request):
    status = 'OK'
    message = ''
    user_id = ''
    user_token = ''
    
    try:
        post = request.POST
        firstname = post.get('firstName',None)
        lastname = post.get('lastName',None)
        password = post.get('password',None)
        email = post.get('email',None)
        phone = post.get('phone',None)
        year_of_birth = post.get('yearOfBirth',None)
        gender = post.get('gender',None)
        
        if all([firstname, lastname, password, email]):
            old_user = User.objects.filter(email = email).first()
            if old_user:
                status = 'FAIL'
                message = 'Email already used'
            else:
                user = User()
                user.firstname = firstname
                user.lastname = lastname
                user.email = email
                user.phone = phone
                user.year_of_birth = year_of_birth
                user.gender = gender
                user.password = get_hash_of_string(password)
                user_uuid = uuid.uuid1()
                user.registration_token = user_uuid.__str__()
                user.save()
                user_id = user.id
                user_token = user.registration_token
        else:
            status = 'Fail'
            message = 'MISSING PARAMS'
    except Exception as e:
        status = 'ERROR'
        message = e.message
    return ajax_response({'status' : status, 'message' : message, 'user_id' : user_id, 'token' : user_token})

@csrf_exempt       
def login_user(request):
    status = 'OK'
    message = ''
    user_id = ''
    user_token = ''
    firstname = ''
    lastname = ''
    try:
        post = request.POST
        email = post.get('email',None)
        password = post.get('password',None)
        if all([password, email]):
            hashed_password = get_hash_of_string(password)
            user = User.objects.filter(email = email, password = hashed_password).first()
            if user:
                user_id = user.pk
                user_token = user.registration_token
                firstname = user.firstname
                lastname = user.lastname
            else:
                status = 'FAIL'
                message = 'Incorrect email or password'
        else:
            status = 'FAIL'
            message = 'MISSING PARAMS'
    except Exception as e:
        status = 'ERROR'
        message = e.message
    return ajax_response({'status' : status, 'message' : message, 'user_id' : user_id, 'token' : user_token, 'firstname':firstname, 'lastname':lastname})

@csrf_exempt
def edit_user_cat(request):
    status = 'OK'
    message = ''
    try:
        post = request.POST
        user_id = post.get('userId')
        user_token = post.get('token')
        added = post.get('added')
        
        if all([user_id, user_token, added]):
            added = json.loads(added)
            user = User.objects.filter(id = user_id).first()
            User_categorie.objects.filter(user_id = user_id).delete()
            for rec in added:
                if rec == 0:
                    continue
                cat = User_categorie()
                cat.category_id = Category.objects.filter(id = rec).first()
                cat.user_id = user
                cat.save()
        else:
            status = 'FAIL'
            message = 'MISSING PARAMS'
        
    except Exception as e:
        status = 'ERROR'
        message = e.message
    return ajax_response({'status':status, 'message':message})

@csrf_exempt
def get_user_cat(request):
    status = 'OK'
    message = ''
    data = []
    try:
        post = request.GET
        user_id = post.get('userId')
        user_token = post.get('token')
        if all([user_id, user_token]):
            user = User.objects.filter(id = user_id, registration_token = user_token).first()
            if user:
                favs = User_categorie.objects.filter(user_id = user_id)
                for fav in favs:
                    category = fav.category_id
                    cat_id = category.pk
                    name = category.name
                    img = category.cat_image.url
                    count = Sub_category.objects.filter(category_id = category.pk).count()
                    data.append({'id' : cat_id , 'name': name, 'image' : img, 'count' : count})
                data.append({'id' : 0 , 'name': 'Other', 'image' : 'media/photos/categories/others.jpg', 'count' : 0})
            else:
                status = 'FAIL'
                message = 'User not found'
        else:
            status = 'FAIL'
            message = 'MISSING PARAMS'
    except Exception as e :
        status = 'ERROR'
        message = e.message
    return ajax_response({'status':status, 'message':message, 'data':data})
        
        
    