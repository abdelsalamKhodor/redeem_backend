'''
Created on Sep 14, 2016

@author: User
'''

from django.conf.urls import url
from redeem.common import tools
import logging
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.http.response import JsonResponse
from redeem.models.relational import Client, Store, Announcer
from django.views.decorators.csrf import csrf_exempt
from redeem.common.constants import group, REDEEM_PROCESS_CHOICES
from django.contrib.auth.models import User, Group
from redeem.common.tools import get_hash_of_string
log = logging.getLogger(__name__)

def get_stores_page(request):
    try:
        ctx = tools.defaultContextData(request)
        return render_to_response('store/stores.html',ctx)
    except Exception as e:
        log.error("An error occurred while get stores page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
def stores_index(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        user_group = user.groups.values_list('name',flat=True)
        group_list = [str(x) for x in user_group]
        if group.CLIENT in group_list:
            client = Client.objects.filter(user = user).first()
            ctx['client'] = client
        return render_to_response('store/index.html', ctx) 
    except Exception as e:
        log.error("An error occurred while get store index page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def stores_grid(request):
    try:
        ctx = tools.defaultContextData(request)
#         user = request.user
#         if user.is_superuser :
#             ctx['group'] = 'admin'
        return render_to_response('store/stores_grid.html',ctx)
    except Exception as e:
        log.error("An error occurred while get deals grid page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def get_stores(request):
    try:
        user = request.user
        stores = None
        user_group = user.groups.values_list('name',flat=True)
        group_list = [str(x) for x in user_group]
        if user.is_superuser:
            stores = Store.objects.all()
        elif group.ANNOUNCER in group_list:
            announcer = Announcer.objects.filter(user = user).first()
            clients = Client.objects.filter(announcer = announcer)
            stores = Store.objects.filter(client__in = clients)
        
        elif group.CLIENT in group_list:
            client = Client.objects.filter(user = user).first()
            stores = Store.objects.filter(client = client)
        
        rows = []
        for cat in stores:
            rows.append({'id' : cat.pk,'name' : cat.name, 'client' : cat.client.name})
        return JsonResponse({'rows' : rows})
    except Exception as e:
        log.error("An error occurred while get cats info : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def store_form(request):
    try:
        store_id = request.GET.get('store_id')
        store = Store.objects.filter(id = store_id).first()
        ctx = tools.defaultContextData(request)
        ctx['store'] = store
        ctx['choices'] = REDEEM_PROCESS_CHOICES
        return render_to_response('store/store_form.html', ctx)
    except Exception as e:
        log.error("An error occurred while get store form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
@csrf_exempt
def save_store_form(request):
    try:
        ctx = tools.defaultContextData(request)
        post = request.POST
        store_id = post.get('id')
        name = post.get('name')
        email = post.get('email')
        phone = post.get('phone')
        username = post.get('username')
        password = post.get('password')
        client_user = ctx['user']
        if store_id:
            edit_store(store_id, name, email, phone)
        
        elif all([name, email, phone, username, password]):
            client = Client.objects.filter(user = client_user).first()
            if client:
                user = User()
                user.username = username
                user.email = email
                hashed_password = get_hash_of_string(password)
                user.password = hashed_password
                user.save()
                
                group = Group.objects.get(name='store')
                user.groups.add(group)
                user.save()
                
                store = Store()
                store.name = name
                store.email = email
                store.phone= phone
                store.user = user
                store.client = client
                
                store.save()
                ctx['client'] = client
        return render_to_response("store/stores.html", ctx)
            
    except Exception as e:
        log.error("An error occurred while submit store form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def edit_store(store_id, name, email, phone):
    if all([store_id, name, email, phone]):
        store = Store.objects.filter(id = store_id).first()
        store.name = name
        store.phone = phone
        store.email = email
        user = store.user
        user.email = email
        user.save()
        store.save()

urls = [
        url (r'^sotres$', get_stores_page, name="stores"),
        url (r'^stores_index$', stores_index, name="stores_index"),
        url (r'^stores_grid$', stores_grid, name="stores_grid"),
        url (r'^get_stores$', get_stores, name="get_stores"),
        url (r'^store_form$', store_form, name="store_form"),
        url (r'^save_store_form$', save_store_form, name="save_store_form"),
        ]