'''
Created on Sep 14, 2016

@author: User
'''

from django.conf.urls import url
from redeem.common import tools
import logging
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.http.response import JsonResponse
from redeem.models.relational import Category
from redeem.forms import SignleDocumentForm
from redeem import settings
import os
from django.views.decorators.csrf import csrf_exempt
log = logging.getLogger(__name__)

def get_cats_page(request):
    try:
        ctx = tools.defaultContextData(request)
        return render_to_response('category/categories.html',ctx)
    except Exception as e:
        log.error("An error occurred while get deals page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
def cats_index(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        if user.is_superuser:
            ctx['admin'] = 'admin'
        return render_to_response('category/index.html', ctx) 
    except Exception as e:
        log.error("An error occurred while get cats index page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def cats_grid(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        if user.is_superuser :
            ctx['group'] = 'admin'
        return render_to_response('category/cats_grid.html',ctx)
    except Exception as e:
        log.error("An error occurred while get deals grid page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def get_cats(request):
    try:
        user = request.user
        dealsQset = None
        if user.is_superuser:
            dealsQset = Category.objects.all()
        rows = []
        for cat in dealsQset:
            rows.append({'id' : cat.pk,'name' : cat.name})
        return JsonResponse({'rows' : rows})
    except Exception as e:
        log.error("An error occurred while get cats info : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def cat_form(request):
    try:
        cat_id = request.GET.get('cat_id')
        cat = Category.objects.filter(id = cat_id).first()
        ctx = tools.defaultContextData(request)
        ctx['category'] = cat
        ctx['form'] = SignleDocumentForm()
        ctx['MEDIA_URL'] = settings.MEDIA_URL
        return render_to_response('category/cat_form.html', ctx)
    except Exception as e:
        log.error("An error occurred while get cat form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
@csrf_exempt
def save_category_form(request):
    try:
        ctx = tools.defaultContextData(request)
        post = request.POST
        cat_id = post.get('id')
        name = post.get('name')
        form = SignleDocumentForm(request.POST, request.FILES)
        logo_url = None
        if 'docfile' in request.FILES:
            logo_url = request.FILES.get('docfile')
        
        
        if cat_id:
            edit_cat(cat_id, name, logo_url, form)
        
        elif name:
            cat = Category()
            cat.name = name
            if form.is_valid() and logo_url:
                cat.cat_image = logo_url
                cat.cat_image.name = os.path.join('photos/categories',str(cat.name)+'.png')
                fullname = os.path.join(settings.MEDIA_ROOT, cat.cat_image.name)
                if os.path.exists(fullname):
                    os.remove(fullname)
            cat.save()
        return render_to_response("categories.html", ctx)
            
    except Exception as e:
        log.error("An error occurred while submit client form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def edit_cat(cat_id, name, logo, form):
    if all([cat_id, name]):
        cat = Category.objects.filter(id = cat_id).first()
        cat.name = name
        if form.is_valid() and logo:
            cat.cat_image = logo
            fullname = os.path.join(settings.MEDIA_ROOT, cat.cat_image.name)
            if os.path.exists(fullname):
                os.remove(fullname)
        cat.save()

urls = [
        url (r'^categories$', get_cats_page, name="categories"),
        url (r'^cats_index$', cats_index, name="cats_index"),
        url (r'^cats_grid$', cats_grid, name="cats_grid"),
        url (r'^get_cats$', get_cats, name="get_cats"),
        url (r'^cat_form$', cat_form, name="cat_form"),
        url (r'^save_category_form$', save_category_form, name="save_category_form"),
        ]