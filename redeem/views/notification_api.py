'''
Created on Sep 5, 2016

@author: User
'''
from django.conf.urls import url
from redeem.models.relational import User, Deal
from redeem.models.non_relational import Notification
from push_notifications.models import GCMDevice
import logging
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from redeem.common import mongo_tools, tools
from redeem.common.tools import fix_timeStamp
from time import mktime
from django.utils import timezone
from redeem import settings
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse

log = logging.getLogger(__name__)


def send_notification_form(request):
    try:
        ctx = tools.defaultContextData(request)
        ctx['deal_id'] = request.GET.get('deal_id')
        return render_to_response('deal/send_notification_form.html', ctx)
    except Exception as e:
        log.error("An error occurred while get notification form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

@csrf_exempt
def submit_send_notification(request):
    try:
        deal_id = request.POST.get('deal_id')
        email = request.POST.get('email')
        deal = Deal.objects.filter(id = deal_id).first()
        if all([deal, email]):
            img_url = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'
            if deal.image:
                img_url = settings.SERVER_URL + deal.image.url
            send_notification(email,deal_id, deal.summary, deal.details, img_url)
        return JsonResponse({'status' : 'OK'})
    except Exception as e:
        log.error("An error occurred while submit notification form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

@csrf_exempt
def receive_notification_token(request):
    try:
        post = request.POST
        user_id = post.get('userId')
        user_token = post.get('token')
        notification_token = post.get('notificationToken')
        
        if all([user_id, user_token, notification_token]):
            user = User.objects.filter(id = user_id).first()
            if user :
                name = user.firstname + user.lastname
                User.objects.filter(pk = user.pk).update(notification_token = notification_token)
                GCMDevice.objects.update_or_create(name=name, device_id=user_id,defaults={'registration_id': notification_token, 'active' : True})
                message= {"status":"OK","message":"Data saved successfully"}
            else:
                message= {"status":"ERROR","message":"user not found"}
        else:
            log.error("Missing PARAMS %s", exc_info=True)
            message = {'status': 'ERROR', 'message': 'Missing PARAMS'}
    except:
        log.error("Unknown Error : %s", exc_info=True)
        message = {'status': 'FAIL', 'message': 'Error while receiving notification token'} 
         
    return JsonResponse(message)


def send_notification(email, deal_campaign_id, summary, details, image):
    sent = True
    user_id = None
    try:
        if all([email, summary]):
            user = User.objects.filter(email = email).first()
            if user:
                device = GCMDevice.objects.filter(registration_id = user.notification_token)
                
                if len(device) == 0:
                    message= {"status":"ERROR","message":"user not registered"}
                    sent = False
                else:
                    device.send_message(summary, extra={"details": details, 'flag' : 'show', 'type' : 'textWithImage', 'logo' : image, 'deal_id' : deal_campaign_id})
                    user_id = str(user.pk)
                    message= {"status":"OK","message":"notification sent successfully"}
            else:
                message= {"status":"ERROR","message":"user not found"}
                sent = False
        else:
                log.error("Missing notification token or phone number or client %s", exc_info=True)
                message = {'status': 'ERROR', 'message': 'Missing notification params'}
                sent = False
    except:
        log.error("Unknown Error : %s", exc_info=True)
        message = {'status': 'FAIL', 'message': 'Error while sending notification'} 
        sent = False
    if sent == True:
        creation_time = timezone.now()
        db = mongo_tools.get_db()
        db.notification.insert({'user_id' : user_id, 'deal_campaign_id' : deal_campaign_id, 'creation_time' : creation_time})
        mongo_tools.close(db)
    return JsonResponse(message)


@csrf_exempt
def receive_notification_status(request):
    db = mongo_tools.get_db()
    try:
        response_status = 'OK'
        post = request.POST
        user_id = post.get('userId', None)
        deal_id = post.get('dealId', None)
#         user_token = post.get('token', None)
        timestamp = post.get('userTimeStamp', None)
        status = post.get('notificationStatus', None)
        
        if all([user_id, deal_id, timestamp, status]):
            timestamp = fix_timeStamp(timestamp)
            if status == 'delivered':
                db.notification.update_one({'user_id':user_id, 'deal_campaign_id' : deal_id}, {"$set": {"delivery_time": timestamp}})
            elif status == 'seen':
                db.notification.update_one({'user_id':user_id, 'deal_campaign_id' : deal_id}, {"$set": {"seen_time": timestamp}})
            elif status == 'used':
                db.notification.update_one({'user_id':user_id, 'deal_campaign_id' : deal_id}, {"$set": {"use_time": timestamp}})
        else:
            response_status = 'missing data'
    except:
        log.error("error when saving notification status", exc_info=True)
        response_status = 'Failed'
    mongo_tools.close(db)
    return JsonResponse({'status' : response_status})

@csrf_exempt    
def get_notifications_list_by_user(request):
    try:
        result = {'status' :'OK', 'message' : ''}
        data = request.GET
        user_id = data.get('userId', None)
#         user_token = post.get('userToken', None)
        if user_id:
            notifications = Notification.objects.filter(user_id = user_id).order_by("-_id")
            data = []
            if notifications:
                for notification in notifications:
                    deal_id = notification.deal_campaign_id
                    deal = Deal.objects.filter(id = int(deal_id)).first()
                    if deal:
                        img_url = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'
                        if deal.image:
                            img_url = deal.image.url
                        expire_date = deal.expiry_date
                        creation_time = notification.creation_time
                        creation_time_long = int (mktime(creation_time.timetuple())+1e-6*creation_time.microsecond) * 1000
                        data.append({'dealLogoUrl' : img_url, 'dealMessage' : deal.summary, 'creationTime' : creation_time_long, 'deal_id' : notification.deal_campaign_id, 'expire_date' : str(expire_date)})
            result['data'] = data
            return JsonResponse(result)
    except Exception as e :
        log.error("error when get notification list", exc_info=True)
        return JsonResponse({'status' : 'Failed', 'message' : e.message})

def get_notification_details(request):
    try:
        result = {'status' :'OK', 'message' : ''}
        deal_id = request.GET.get('dealId', None)
        if deal_id:
            deal = Deal.objects.filter(id = deal_id).first()
            if deal:
                expiry_date = deal.expiry_date
                expire_date_str = ''
                if expiry_date:
                    expire_date_str = expiry_date.date()
                banner = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'
                if deal.banner:
                    banner = deal.banner.url
                result['banner'] = banner
                result['details'] = deal.details
                result['creation_time'] = deal.cration_date
                result['expiry_date'] = expire_date_str
    except Exception as e :
        log.error("error when get notification info", exc_info=True)
        return JsonResponse({'status' : 'Failed', 'message' : e.message})
    return JsonResponse(result)

urls = [
         url (r'^send_notification_form$', send_notification_form, name="send_notification_form"),
         url (r'^submit_send_notification$', submit_send_notification, name="submit_send_notification"),
        ]