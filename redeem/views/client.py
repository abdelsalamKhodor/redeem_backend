'''
Created on Sep 8, 2016

@author: User
'''
from django.shortcuts import render_to_response, redirect
from redeem.common import tools
from django.conf.urls import url
from django.core.urlresolvers import reverse
from redeem.models.relational import Announcer, Client, Deal
import logging
from redeem.forms import SignleDocumentForm
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from redeem.common.tools import get_hash_of_string
from django.contrib.auth.models import User, Group
from redeem.common.constants import group
import os
from redeem import settings

log = logging.getLogger(__name__)

def clients_grid(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        if user.is_superuser :
            ctx['group'] = 'admin'
        return render_to_response('client/client_grid.html',ctx)
    except Exception as e:
        log.error("An error occurred while get clients grid page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def get_clients(request):
    try:
        user_id = request.GET.get('user_id')
        user = User.objects.filter(id = user_id).first()
        user_group = user.groups.values_list('name',flat=True)
        group_list = [str(x) for x in user_group]
        clientsQset = None
        if group.ANNOUNCER in group_list:
            announcer = Announcer.objects.filter(user = user)
            clientsQset = Client.objects.filter(announcer = announcer)
        if user.is_superuser:
            clientsQset = Client.objects.all()
        rows = []
        for obj in clientsQset:
            announcer = ''
            if obj.announcer:
                announcer = obj.announcer.name
            rows.append({'id' : obj.pk,'name' : obj.name, 'phone':obj.phone, 'email':obj.email, 'announcer' : announcer})
        return JsonResponse({'rows' : rows})
    except Exception as e:
        log.error("An error occurred while get clients info : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def client_form(request):
    try:
        client_id = request.GET.get('id')
        client = Client.objects.filter(id = client_id).first()
        user = request.user
        announcer = Announcer.objects.filter(user = user).first()
        ctx = tools.defaultContextData(request)
        ctx['client'] = client
        ctx['announcer'] = announcer
        ctx['form'] = SignleDocumentForm()
        ctx['MEDIA_URL'] = settings.MEDIA_URL
        return render_to_response('client/client_form.html', ctx)
    except Exception as e:
        log.error("An error occurred while get client form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

@csrf_exempt
def save_client_form(request):
    try:
        ctx = tools.defaultContextData(request)
        post = request.POST
        client_id = post.get('id')
        announcer_id = post.get('announcer_id')
        name = post.get('client_name')
        email = post.get('email')
        phone = post.get('phone')
        username = post.get('username')
        password = post.get('password')
        physical = 1 if post.get('physical')=='on' else 0
        salesmen = 1 if post.get('salesmen')=='on' else 0
        announcer_user = ctx['user']
        form = SignleDocumentForm(request.POST, request.FILES)
        logo_url = None
        if 'docfile' in request.FILES:
            logo_url = request.FILES.get('docfile')
        
        
        if client_id:
            edit_client(client_id, name, email, phone,physical, salesmen, logo_url, form)
        
        elif all([name, email, phone, username, password, announcer_id]):
            announcer = Announcer.objects.filter(user = announcer_user).first()
            if announcer:
                user = User()
                user.username = username
                user.email = email
                hashed_password = get_hash_of_string(password)
                user.password = hashed_password
                user.save()
                
                group = Group.objects.get(name='client')
                user.groups.add(group)
                user.save()
                
                client = Client()
                client.name = name
                client.email = email
                client.phone= phone
                client.user = user
                client.announcer = announcer
                client.physical = physical
                client.salesmen = salesmen
                
                if form.is_valid() and logo_url:
                    client.logo = logo_url
                    client.logo.name = os.path.join('photos/clients',str(client.name)+'.png')
                    fullname = os.path.join(settings.MEDIA_ROOT, client.logo.name)
                    if os.path.exists(fullname):
                        os.remove(fullname)
                client.save()
        ctx['announcer'] = 'announcer'
        ctx['active_tab'] = 'client'
        ctx['group'] = 'announcer'
        return render_to_response("users.html", ctx)
            
    except Exception as e:
        log.error("An error occurred while submit client form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def edit_client(client_id, name, email, phone, physical, salesmen, logo, form):
    if all([client_id, name, email, phone]):
        client = Client.objects.filter(id = client_id).first()
        client.name = name
        client.phone = phone
        client.email = email
        client.physical = physical
        client.salesmen = salesmen
        user = client.user
        user.email = email
        user.save()
        if form.is_valid() and logo:
            client.logo = logo
            fullname = os.path.join(settings.MEDIA_ROOT, client.logo.name)
            if os.path.exists(fullname):
                os.remove(fullname)
        client.save()

def delete_client_form(request):
    try:
        ctx = tools.defaultContextData(request)
        client_id = request.GET.get('id')
        client = Client.objects.filter(id = client_id).first()
        ctx['client_id'] = client_id
        deals = Deal.objects.filter(client = client )
        if deals:
            ctx['can_delete'] = False
        else:
            ctx['can_delete'] = True   
        return render_to_response('client/client_delete_form.html',ctx)
    except Exception as e:
        log.error("An error occurred while get delete client form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

@csrf_exempt
def delete_client(request):
    try:
        client_id = request.POST.get('id')
        client = Client.objects.filter(id = client_id).first()
        user = client.user
        user.delete()
        client.delete()
        return JsonResponse({'status' : 'OK'})
    except Exception as e:
        log.error("An error occurred while delete client : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

urls = [
        url (r'^clients_grid$', clients_grid, name="clients_grid"),
        url (r'^get_clients$', get_clients, name="get_clients"),
        url (r'^client_form$', client_form, name="client_form"),
        url (r'^save_client_form$', save_client_form, name="save_client_form"),
        url (r'^delete_client_form$', delete_client_form, name="delete_client_form"),
        url (r'^delete_client$', delete_client, name="delete_client"),
        ]