'''
Created on Aug 26, 2016

@author: User
'''
from django.shortcuts import render_to_response, redirect
from redeem.common import tools
from django.conf.urls import url
from django.core.urlresolvers import reverse
import logging
from django.views.decorators.csrf import csrf_exempt
from redeem.common.constants import group
from django.contrib.auth import views
from django.contrib.auth.decorators import login_required

log = logging.getLogger(__name__)

def get_error_page(request):
    ctx = tools.defaultContextData(request)
    return render_to_response('page_error.html',ctx)

def get_forbidden_page(request):
    ctx = tools.defaultContextData(request)
    return render_to_response('forbidden.html',ctx)

def get_login_page(request):
    try:
        ctx = tools.defaultContextData(request)
        return render_to_response('account/login.html',ctx)
    except Exception as e:
        log.error("An error occurred while get login page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

@login_required(login_url='/')
@csrf_exempt
def get_home_page(request):
    try:
        user = request.user
        user_group = user.groups.values_list('name',flat=True)
        group_list = [str(x) for x in user_group]
        
        if group.CLIENT in group_list:
            return redirect(reverse('deals'))
        
        if group.ANNOUNCER in group_list or user.is_superuser:
            return redirect(reverse('users'))
        
    except Exception as e:
        log.error("An error occurred while get home page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

@login_required(login_url='/')
def get_users_page(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        user_group = user.groups.values_list('name',flat=True)
        group_list = [str(x) for x in user_group]
        if group.ANNOUNCER in group_list:
            ctx['announcer'] = 'announcer'
            ctx['active_tab'] = 'client'
            ctx['group'] = 'announcer'
        if user.is_superuser:
            ctx['active_tab'] = 'announcer'
        return render_to_response('users.html',ctx)
    except Exception as e:
        log.error("An error occurred while get users page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def switch_tab(request):
    try:
        ctx = tools.defaultContextData(request)
        role = request.GET.get('tab_role')
        user = request.user
        tab_file = ''
        if role=="announcer":
            tab_file = 'announcer/index.html'
        elif role == 'client':
            if user.is_superuser :
                ctx['admin'] = 'admin'
            else :
                ctx['announcer'] = 'announcer'
            tab_file = 'client/index.html'
        return render_to_response(tab_file, ctx) 
    except Exception as e:
        log.error("An error occurred while switch tab : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

urls = [
        url(r'^$', views.login,  name='login', kwargs={"template_name":"account/login.html","extra_context":{"next":"home"}}),
        url(r'^logout$', views.logout,{'next_page': 'login'}, name='logout'),
        url(r'^error_page$', get_error_page, name="error_page"),
        url(r'^forbidden_page$', get_forbidden_page, name="forbidden_page"),
        url(r'home$',get_home_page,name="home"),
        url(r'users$',get_users_page,name="users"),
        url (r'^switch_tab$', switch_tab, name="switch_tab"),
    ]