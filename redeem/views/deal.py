'''
Created on Sep 8, 2016

@author: User
'''

from django.shortcuts import render_to_response, redirect
from redeem.common import tools
from django.conf.urls import url
from django.core.urlresolvers import reverse
from redeem.models.relational import Announcer, Client, Deal
import logging
from redeem.forms import TwoDocumentForm
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from redeem.common.constants import group
from django.contrib.auth.decorators import login_required
import os
from redeem import settings

log = logging.getLogger(__name__)
@login_required(login_url='/')
def get_deals_page(request):
    try:
        ctx = tools.defaultContextData(request)
        return render_to_response('deal/deals.html',ctx)
    except Exception as e:
        log.error("An error occurred while get deals page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def deals_index(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        user_group = user.groups.values_list('name',flat=True)
        group_list = [str(x) for x in user_group]
        if user.is_superuser:
            ctx['admin'] = 'admin'
        elif group.ANNOUNCER in group_list:
            ctx['announcer'] = 'announcer'
        elif group.CLIENT in group_list:
            ctx['client'] = 'client'
        return render_to_response('deal/index.html', ctx) 
    except Exception as e:
        log.error("An error occurred while get deals index page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))


def deals_grid(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        if user.is_superuser :
            ctx['group'] = 'admin'
        return render_to_response('deal/deal_grid.html',ctx)
    except Exception as e:
        log.error("An error occurred while get deals grid page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def get_deals(request):
    try:
        user = request.user
        user_group = user.groups.values_list('name',flat=True)
        group_list = [str(x) for x in user_group]
        dealsQset = None
        if group.CLIENT in group_list:
            client = Client.objects.filter(user = user).first()
            dealsQset = Deal.objects.filter(client = client)
        elif group.ANNOUNCER in group_list:
            dealsQset = Deal.objects.filter(user = user)
        elif user.is_superuser:
            dealsQset = Deal.objects.all()
        rows = []
        for obj in dealsQset:
            client = ''
            if obj.client:
                client = obj.client.name
            rows.append({'id' : obj.pk,'name' : obj.name, 'summary':obj.summary, 'client' : client})
        return JsonResponse({'rows' : rows})
    except Exception as e:
        log.error("An error occurred while get deals info : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def deal_form(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        user_group = user.groups.values_list('name',flat=True)
        group_list = [str(x) for x in user_group]
        if group.ANNOUNCER in group_list:
            announcer = Announcer.objects.filter(user = user).first()
            clients = Client.objects.filter(announcer = announcer)
            ctx['clients'] = clients
            ctx['announcer'] = announcer
        elif group.CLIENT in group_list:
            client = Client.objects.filter(user = user).first()
            ctx['client'] = client
        ctx['form'] = TwoDocumentForm()
        ctx['MEDIA_URL'] = settings.MEDIA_URL
        return render_to_response('deal/deal_form.html', ctx)
    except Exception as e:
        log.error("An error occurred while get deal form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))


@csrf_exempt
def save_deal_form(request):
    try:
#         ctx = tools.defaultContextData(request)
        post = request.POST
        user = request.user
        name = post.get('deal_name')
        summary = post.get('summary')
        details = post.get('details')
        color = post.get('color')
        text_color = post.get('text_color')
        language = post.get('language')
        client_id = post.get('client_id')
        if not client_id:
            client_id = post.get('hidden_client_id')
        client = Client.objects.filter(id = client_id).first()
        form = TwoDocumentForm(request.POST, request.FILES)
#         image1_url = request.FILES.get('image1', None)
        
        if all([name, summary, client]):
            deal = Deal()
            deal.name = name
            deal.summary = summary
            deal.details = details
            deal.color = color
            deal.text_color = text_color
            deal.language = language
            deal.client = client
            deal.user = user
            
            if form.is_valid():
                image1 = None
                if 'image1' in request.FILES:
                    image1 = request.FILES.get('image1')
                    deal.image = image1
                    fullname = os.path.join(settings.MEDIA_ROOT, deal.name)
                    if os.path.exists(fullname):
                        os.remove(fullname)
                
                image2 = None
                if 'image2' in request.FILES:
                    image2 = request.FILES.get('image2')
                    deal.banner = image2
                    fullname = os.path.join(settings.MEDIA_ROOT, deal.name)
                    if os.path.exists(fullname):
                        os.remove(fullname)
                    
            deal.save()
        return redirect(reverse('deals'))
    except Exception as e:
        log.error("An error occurred while submit deal form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def view_deal(request):
    try:
        ctx = tools.defaultContextData(request)
        deal_id = request.GET.get('deal_id')
        deal = Deal.objects.filter(id = deal_id).first()
        if deal:
            ctx['MEDIA_URL'] = settings.MEDIA_URL
            ctx['deal'] = deal
        return render_to_response('deal/deal_details.html', ctx)
    except Exception as e:
        log.error("An error occurred while get deal details : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

urls = [
        url (r'^deals$', get_deals_page, name="deals"),
        url (r'^deals_index$', deals_index, name="deals_index"),
        url (r'^deals_grid$', deals_grid, name="deals_grid"),
        url (r'^get_deals$', get_deals, name="get_deals"),
        url (r'^deal_form$', deal_form, name="deal_form"),
        url (r'^save_deal_form$', save_deal_form, name="save_deal_form"),
        url (r'^view_deal$', view_deal, name="view_deal"),
        ]