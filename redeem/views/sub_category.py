'''
Created on Sep 15, 2016

@author: User
'''

'''
Created on Sep 14, 2016

@author: User
'''

from django.conf.urls import url
from redeem.common import tools
import logging
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.http.response import JsonResponse
from redeem.models.relational import Category, Sub_category
from redeem.forms import SignleDocumentForm
from redeem import settings
import os
from django.views.decorators.csrf import csrf_exempt
log = logging.getLogger(__name__)

def get_sub_cats_page(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        if user.is_superuser:
            ctx['admin'] = 'admin'
            return render_to_response('sub_category/sub_cats.html',ctx)
        else:
            return redirect(reverse('forbidden_page'))
    except Exception as e:
        log.error("An error occurred while get sub categories page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
def sub_cats_index(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        if user.is_superuser:
            ctx['admin'] = 'admin'
        return render_to_response('sub_category/index.html', ctx) 
    except Exception as e:
        log.error("An error occurred while get sub category index page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def sub_cats_grid(request):
    try:
        ctx = tools.defaultContextData(request)
        user = request.user
        if user.is_superuser :
            ctx['group'] = 'admin'
        return render_to_response('sub_category/sub_cats_grid.html',ctx)
    except Exception as e:
        log.error("An error occurred while get sub categories grid page : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def get_sub_cats(request):
    try:
        user = request.user
        dealsQset = None
        if user.is_superuser:
            dealsQset = Sub_category.objects.all()
        rows = []
        for cat in dealsQset:
            rows.append({'id' : cat.pk,'name' : cat.name, 'category' : cat.category.name})
        return JsonResponse({'rows' : rows})
    except Exception as e:
        log.error("An error occurred while get sub categories info : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))

def sub_cat_form(request):
    try:
        sub_cat_id = request.GET.get('sub_cat_id')
        sub_cat = Sub_category.objects.filter(id = sub_cat_id).first()
        all_cats = []
        if sub_cat:
            all_cats = Category.objects.all().exclude(id = sub_cat.category.id)
        else:
            all_cats = Category.objects.all()
        ctx = tools.defaultContextData(request)
        ctx['sub_cat'] = sub_cat
        ctx['categories'] = all_cats
        ctx['form'] = SignleDocumentForm()
        ctx['MEDIA_URL'] = settings.MEDIA_URL
        return render_to_response('sub_category/sub_cat_form.html', ctx)
    except Exception as e:
        log.error("An error occurred while get sub category form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
@csrf_exempt
def save_sub_cat_form(request):
    try:
        ctx = tools.defaultContextData(request)
        post = request.POST
        sub_cat_id = post.get('id')
        name = post.get('name')
        cat_id = post.get('cat_id')
        cat = Category.objects.filter(id = cat_id).first()
        
        form = SignleDocumentForm(request.POST, request.FILES)
        logo_url = None
        if 'docfile' in request.FILES:
            logo_url = request.FILES.get('docfile')
        
        
        if sub_cat_id:
            edit_cat(sub_cat_id, name,cat,  logo_url, form)
        
        elif all([name, cat]):
            sub_cat = Sub_category()
            sub_cat.name = name
            sub_cat.category = cat
            if form.is_valid() and logo_url:
                sub_cat.sub_cat_image = logo_url
                sub_cat.sub_cat_image.name = os.path.join('photos/categories',str(sub_cat.name), str(sub_cat.name)+'.png')
                fullname = os.path.join(settings.MEDIA_ROOT, sub_cat.sub_cat_image.name)
                if os.path.exists(fullname):
                    os.remove(fullname)
            sub_cat.save()
        return render_to_response("sub_cats.html", ctx)
            
    except Exception as e:
        log.error("An error occurred while submit sub category form : " + e.message, exc_info=True)
        return redirect(reverse('error_page'))
    
def edit_cat(sub_cat_id, name,cat, logo, form):
    if all([sub_cat_id, name, cat]):
        sub_cat = Sub_category.objects.filter(id = sub_cat_id).first()
        sub_cat.name = name
        sub_cat.category = cat
        if form.is_valid() and logo:
            sub_cat.sub_cat_image = logo
            fullname = os.path.join(settings.MEDIA_ROOT, sub_cat.sub_cat_image.name)
            if os.path.exists(fullname):
                os.remove(fullname)
        sub_cat.save()

urls = [
        url (r'^sub_categories$', get_sub_cats_page, name="sub_categories"),
        url (r'^sub_cats_index$', sub_cats_index, name="sub_cats_index"),
        url (r'^sub_cats_grid$', sub_cats_grid, name="sub_cats_grid"),
        url (r'^get_sub_cats$', get_sub_cats, name="get_sub_cats"),
        url (r'^sub_cat_form$', sub_cat_form, name="sub_cat_form"),
        url (r'^save_sub_cat_form$', save_sub_cat_form, name="save_sub_cat_form"),
        ]
