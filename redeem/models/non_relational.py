'''
Created on Sep 7, 2016

@author: User
'''
from mongoengine.document import DynamicDocument
from mongoengine.fields import StringField, DateTimeField

class Notification(DynamicDocument):
    user_id = StringField(max_length= 32)
    deal_campaign_id = StringField(max_length= 32)
    creation_time = DateTimeField()
    delivery_time = DateTimeField()
    seen_time = DateTimeField()
    use_time =  DateTimeField()
    
    meta = {'collection': 'notification'}