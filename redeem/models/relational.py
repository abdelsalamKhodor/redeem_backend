'''
Created on Aug 22, 2016

@author: User
'''
from django.db import models
import os
from django.db.models.fields.files import ImageField
from django.contrib import admin
from django.contrib.auth.models import User
from redeem.common.constants import LANGUAGE_CHOICES, REDEEM_PROCESS_CHOICES

def get_cat_image_path(instance, filename):
    return os.path.join('photos/categories', str(instance.name)+".png")

def get_sub_cat_image_path(instance, filename):
    return os.path.join('photos/categories', str(instance.category.name),str(instance.name), filename)

def get_deal_image_path(instance, filename):
    return os.path.join('photos/deals', str(instance.client.name), 'image', str(instance.name)+'.png')

def get_deal_banner_path(instance, filename):
    return os.path.join('photos/deals', str(instance.client.name), 'banner', str(instance.name)+'.png')


def get_client_logo_path(instance, filename):
    return os.path.join('photos/clients', str(instance.name), 'logo.png')

def get_announcer_logo_path(instance, filename):
    return os.path.join('photos/announcers',str(instance.name), 'logo.png')

class Category(models.Model):
    name = models.CharField(max_length=64, verbose_name = 'Name')
    cat_image = ImageField(upload_to=get_cat_image_path, blank=True, null=True)
    def __str__(self):             
        return str(self.name)

class Sub_category(models.Model):
    name = models.CharField(max_length=64, verbose_name = 'Name')
    sub_cat_image = ImageField(upload_to=get_sub_cat_image_path, blank=True, null=True)
    category = models.ForeignKey(Category, verbose_name = 'Category', blank=True, null=True)
    def __str__(self):             
        return str(self.name)

class Country(models.Model):
    name = models.CharField(max_length=50,default = '', verbose_name = 'Name')
    mcc = models.CharField(max_length=8,default = '000', verbose_name = 'MCC')
    def __str__(self):
        return str(self.name)

class City(models.Model):
    name = models.CharField(max_length=50,default = '', verbose_name = 'Name')
    country = models.ForeignKey(Country, blank=True, null=True, verbose_name = 'Country')
    def __str__(self):
        return str(self.name)

class Announcer(models.Model):
    name = models.CharField(max_length=64, verbose_name = 'Name')
    logo_url = models.ImageField(upload_to=get_announcer_logo_path,blank=True, null=True)
    email = models.EmailField(max_length=254, blank=True, null=True)
    phone = models.CharField(max_length=256, blank=True, null=True)
    user = models.OneToOneField(User, blank=True, null=True)
    
    
    def __str__(self):             
        return str(self.name)

class Client(models.Model):
    name = models.CharField(max_length=64, verbose_name = 'Name')
    logo = ImageField(upload_to=get_client_logo_path, blank=True, null=True)
    announcer = models.ForeignKey(Announcer, blank=True, null=True,  verbose_name = 'Announcer')
    email = models.EmailField(max_length=254, blank=True, null=True)
    phone = models.CharField(max_length=256, blank=True, null=True)
    physical = models.BooleanField(default=False)
    salesmen = models.BooleanField(default=False)
    user = models.OneToOneField(User, blank=True, null=True)
    
    def __str__(self):             
        return str(self.name)

class Store(models.Model):
    name = models.CharField(max_length=64, verbose_name = 'Name')
    client = models.ForeignKey(Client, verbose_name = 'Client', blank=True, null=True)
    email = models.EmailField(max_length=254, blank=True, null=True)
    phone = models.CharField(max_length=256, blank=True, null=True)
    city = models.ForeignKey(City, verbose_name = 'City', blank=True, null=True)
    longitude = models.DecimalField(max_digits=11, decimal_places=8, verbose_name = 'Longitude', blank=True, null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=8, verbose_name = 'Latitude', blank=True, null=True)
    redeem_process = models.CharField(max_length=8, choices=REDEEM_PROCESS_CHOICES, blank=True, null=True,)
    user = models.OneToOneField(User, blank=True, null=True)
    def __str__(self):             
        return str(self.name)

class Deal(models.Model):
    name = models.CharField(max_length=64, verbose_name = 'Name')
    summary = models.CharField(max_length=256, verbose_name = 'summary', blank=True, null=True)
    details = models.CharField(max_length=512, verbose_name = 'details', blank=True, null=True)
    client = models.ForeignKey(Client, verbose_name = 'Client', blank=True, null=True)
    user = models.ForeignKey(User, verbose_name = 'user', blank=True, null=True)
    cration_date = models.DateTimeField(auto_now_add=True, verbose_name = 'Create date')
    expiry_date = models.DateTimeField( blank=True, null=True, verbose_name = 'Expire date')
    sub_category = models.ForeignKey(Sub_category, verbose_name = 'Sub Category', blank=True, null=True)
    image = ImageField(upload_to=get_deal_image_path, blank=True, null=True)
    banner = ImageField(upload_to=get_deal_banner_path, blank=True, null=True)
    color = models.CharField(max_length=8, verbose_name = 'color', blank=True, null=True)
    text_color = models.CharField(max_length=8, verbose_name = 'text_color', blank=True, null=True)
    language = models.CharField(max_length=2, choices=LANGUAGE_CHOICES,blank=True, null=True,)
    def __str__(self):             
        return str(self.name)

class User(models.Model):
    MALE = 'male'
    FEMALE = 'female'
    GENDER_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )
    
    
    firstname = models.CharField(max_length=64, verbose_name = 'First name', blank=True, null=True)
    lastname = models.CharField(max_length=64, verbose_name = 'Last name', blank=True, null=True)
    password = models.CharField(max_length=64, verbose_name = 'password', blank=True, null=True)
    registration_token = models.CharField(max_length=64, verbose_name = 'Token',blank=True, null=True)
    notification_token = models.CharField(max_length=256, verbose_name = 'Notification Token',blank=True, null=True)
    email = models.EmailField(verbose_name = 'email', blank=True, null=True)
    phone = models.CharField(max_length=32, verbose_name = 'Phone', blank=True, null=True)
    year_of_birth = models.IntegerField(verbose_name = 'Date of birth', blank=True, null=True)
    gender = models.CharField(max_length=8, choices=GENDER_CHOICES, default=MALE, blank=True, null=True,)
    country = models.ForeignKey(Country, blank=True, null=True, verbose_name = 'Country')
    language = models.CharField(max_length=2, choices=LANGUAGE_CHOICES,blank=True, null=True,)
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name = 'Create date', blank=True, null=True,)
    
    def __str__(self):             
        return "%s %s"%(self.firstname, self.lastname)

class Favorites(models.Model):
    user_id = models.ForeignKey(User, verbose_name = 'User')
    category_id = models.ForeignKey(Sub_category, verbose_name = 'Category')

class User_categorie(models.Model):
    user_id = models.ForeignKey(User, verbose_name = 'User')
    category_id = models.ForeignKey(Category, verbose_name = 'Category')
    
class Wifi(models.Model):
    ssid = models.CharField(max_length=64, verbose_name = 'SSID')
    mac = models.CharField(max_length=64, verbose_name = 'MAC')
    store = models.ForeignKey(Store, verbose_name = 'Store', blank=True, null=True)
    
    def __str__(self):             
        return str(self.ssid)
    
class Campaign(models.Model):
    pass

admin.site.register(Category)
admin.site.register(Sub_category)
admin.site.register(Announcer)
admin.site.register(Client)
admin.site.register(User)
admin.site.register(Deal)
admin.site.register(Favorites)
admin.site.register(User_categorie)
admin.site.register(Store)
admin.site.register(Wifi)

    