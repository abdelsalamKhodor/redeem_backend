
PROJECT_NAME="redeem_backend"
PROJECT_HEAD=-b master

USER_NAME=redeem #$PROJECT_NAME
GROUP_NAME=redeem #$PROJECT_NAME

APPLY_DB_MIGRATE=1

echo ""
echo "-------------------------------------------------------------Script started on $(date)-----------------------------------------------------------------"

print_usage_exit()
{
 prj=$1
  echo "Usage: '$prj' [-h] -p <project name> -b <branch name>"
  echo "    -h print this help message and exists"
  echo "    -b select branch name"
  echo "    -d skip updating DB. Used on auto start for webschedulers"
  exit 1
}

while getopts hdsb: arg
 do
  case $arg in
   h) print_usage_exit $0;;
   b) PROJECT_HEAD=$OPTARG;;
   d) APPLY_DB_MIGRATE=0;;
   *) print_usage_exit $0;;
 esac   
 done
 
 if ! [ -d /home/$PROJECT_NAME ] 
   then
   echo "ERROR: Couldn't find project $PROJECT_NAME under /home"
   print_usage_exit
  fi


cd /home/$PROJECT_NAME/redeem

echo "Pulling code from repository into `pwd`"
git reset --hard HEAD
git pull

git checkout $PROJECT_HEAD

git pull
git reset --hard FETCH_HEAD

git status

#Give execute permision
chmod u+x /home/$PROJECT_NAME/redeem/server-config/*.sh

echo "creating static contents and migrating db scripts"
cd /home/$PROJECT_NAME
python manage.py collectstatic --noinput > /dev/null
	
if [[ "$APPLY_DB_MIGRATE" == "1" && "$(python manage.py migrate --list | grep '\[ \]' | wc -l)" -ne "0" ]]
	then
		python manage.py migrate
fi 

#Verify/create logging dir
mkdir /var/log/$PROJECT_NAME &> /dev/null
chown -R $USER_NAME:root /var/log/$PROJECT_NAME
chmod -R ug+rw /var/log/$PROJECT_NAME

chown -R $USER_NAME:root /srv/www/vhosts/redeem_backend/static/
chmod -R ug+rw /srv/www/vhosts/redeem_backend/static/

apache2ctl restart
