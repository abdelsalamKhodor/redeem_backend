var main_module={
	open_modal: function(url,modalSize){
		//Open Modal
		$("#main_modal .modal-content").addClass(modalSize).load(url,function( response, status, xhr ){
			if(status != "error"){
				$('#main_modal').modal({
					backdrop: 'static',
				    keyboard: true
				});
				$("#main_modal").modal('show');
			}
		});
	},
	
	open_deal_details_modal: function(url){
		//Open Modal
		$("#deal-details-modal .modal-content").addClass('modal-sm').load(url,function( response, status, xhr ){
			if(status != "error"){
				$('#deal-details-modal').modal({
					backdrop: 'static',
				    keyboard: true
				});
				$("#deal-details-modal").modal('show');
			}
		});
	},
	
	
	close_modal: function(){
		$("#main_modal").modal('hide');
		setTimeout(function(){$("#main_modal .modal-content").html('')},500);
	}
};