//make the jqgrid like bootstrap data table
var jqgrid_tbdt_modal = {
		init_jqgrid_ui : function(){
			<!-- Responsible for removing jquery grid css and addind another, see this link: http:// theme.stepofweb.com/Epona/v1.3/HTML/shortcodes-tables-jqgrid.html
																									// -->
			/**
			 * @STYLING
			 */
		 	$(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
			$(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
			
			/* $(".ui-dialog").children().removeClass("ui-widget-header"); */
			
			$(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
			$(".ui-jqgrid-pager").removeClass("ui-state-default");
			$(".ui-jqgrid").removeClass("ui-widget-content");
			
			$(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
			$(".ui-pg-div").removeClass().addClass("tile-refresh");
			$(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus");
			$(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil");
			$(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o");
			$(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search");
			$(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh");
			$(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-save").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
			$(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");

			$( ".ui-icon.ui-icon-seek-prev" ).wrap( "" );
			$(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

			$( ".ui-icon.ui-icon-seek-first" ).wrap( "" );
			$(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");		  	

			$( ".ui-icon.ui-icon-seek-next" ).wrap( "" );
			$(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

			$( ".ui-icon.ui-icon-seek-end" ).wrap( "" );
			$(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward"); 

		}
	};