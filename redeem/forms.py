'''
Created on Aug 30, 2016

@author: User
'''
from django import forms
class SignleDocumentForm(forms.Form):
    docfile = forms.ImageField(
       label='Image',
       help_text='max. 42 megabytes',
       required=False,
   )
    
class TwoDocumentForm(forms.Form):
    image1 = forms.ImageField(
       label='Image 1',
       help_text='max. 42 megabytes',
       required=False,
   )
    image2 = forms.ImageField(
       label='Image 2',
       help_text='max. 42 megabytes',
       required=False,
   )