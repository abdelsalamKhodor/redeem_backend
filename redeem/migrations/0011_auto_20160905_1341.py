# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('redeem', '0010_auto_20160902_1231'),
    ]

    operations = [
        migrations.AddField(
            model_name='deal',
            name='color',
            field=models.CharField(max_length=8, null=True, verbose_name=b'color', blank=True),
        ),
        migrations.AddField(
            model_name='deal',
            name='language',
            field=models.CharField(blank=True, max_length=2, null=True, choices=[(b'EN', b'English'), (b'AR', b'Arabic'), (b'FR', b'French')]),
        ),
        migrations.AddField(
            model_name='deal',
            name='text_color',
            field=models.CharField(max_length=8, null=True, verbose_name=b'text_color', blank=True),
        ),
        migrations.AddField(
            model_name='deal',
            name='user',
            field=models.ForeignKey(verbose_name=b'Client', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='deal',
            name='details',
            field=models.CharField(max_length=512, null=True, verbose_name=b'details', blank=True),
        ),
        migrations.AlterField(
            model_name='deal',
            name='expiry_date',
            field=models.DateTimeField(null=True, verbose_name=b'Expire date', blank=True),
        ),
    ]
