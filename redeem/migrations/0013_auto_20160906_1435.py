# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0012_user_notification_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deal',
            name='user',
            field=models.ForeignKey(verbose_name=b'user', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='notification_token',
            field=models.CharField(max_length=256, null=True, verbose_name=b'Notification Token', blank=True),
        ),
    ]
