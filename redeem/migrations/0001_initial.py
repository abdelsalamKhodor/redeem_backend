# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import redeem.models.relational


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Announcer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
                ('cat_image', models.ImageField(null=True, upload_to=redeem.models.relational.get_cat_image_path, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
                ('logo', models.ImageField(null=True, upload_to=redeem.models.relational.get_client_logo_path, blank=True)),
                ('announcer', models.ForeignKey(verbose_name=b'Announcer', blank=True, to='redeem.Announcer', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Deal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
                ('summary', models.CharField(max_length=256, null=True, verbose_name=b'summary', blank=True)),
                ('details', models.CharField(max_length=512, null=True, verbose_name=b'summary', blank=True)),
                ('cration_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Create date')),
                ('expiry_date', models.DateTimeField(null=True, verbose_name=b'Create date', blank=True)),
                ('image', models.ImageField(null=True, upload_to=redeem.models.relational.get_deal_image_path, blank=True)),
                ('banner', models.ImageField(null=True, upload_to=redeem.models.relational.get_deal_banner_path, blank=True)),
                ('client_id', models.ForeignKey(verbose_name=b'Client', blank=True, to='redeem.Client', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Favorites',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sub_category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
                ('sub_cat_image', models.ImageField(null=True, upload_to=redeem.models.relational.get_sub_cat_image_path, blank=True)),
                ('category', models.ForeignKey(verbose_name=b'Category', blank=True, to='redeem.Category', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
                ('username', models.CharField(max_length=64, verbose_name=b'username')),
                ('password', models.CharField(max_length=64, verbose_name=b'password')),
                ('email', models.EmailField(max_length=254, null=True, verbose_name=b'username', blank=True)),
                ('phone', models.CharField(max_length=32, verbose_name=b'Phone')),
                ('date_of_birth', models.DateField(null=True, verbose_name=b'Date of birth', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='favorites',
            name='category_id',
            field=models.ForeignKey(verbose_name=b'Category', to='redeem.Sub_category'),
        ),
        migrations.AddField(
            model_name='favorites',
            name='user_id',
            field=models.ForeignKey(verbose_name=b'User', to='redeem.User'),
        ),
        migrations.AddField(
            model_name='deal',
            name='sub_category',
            field=models.ForeignKey(verbose_name=b'Sub Category', blank=True, to='redeem.Sub_category', null=True),
        ),
    ]
