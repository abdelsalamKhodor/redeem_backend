# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import redeem.models.relational


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('redeem', '0006_auto_20160829_1600'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='announcer',
            name='user_id',
        ),
        migrations.AddField(
            model_name='announcer',
            name='user',
            field=models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='announcer',
            name='logo_url',
            field=models.ImageField(null=True, upload_to=redeem.models.relational.get_announcer_logo_path, blank=True),
        ),
    ]
