# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0002_auto_20160825_1126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='gender',
            field=models.CharField(default=b'male', max_length=8, null=True, blank=True, choices=[(b'male', b'Male'), (b'female', b'Female')]),
        ),
    ]
