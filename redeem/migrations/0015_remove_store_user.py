# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0014_campaign_city_store_wifi'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='store',
            name='user',
        ),
    ]
