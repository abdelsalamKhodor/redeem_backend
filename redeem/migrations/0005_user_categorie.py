# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0004_user_creation_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='User_categorie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category_id', models.ForeignKey(verbose_name=b'Category', to='redeem.Category')),
                ('user_id', models.ForeignKey(verbose_name=b'User', to='redeem.User')),
            ],
        ),
    ]
