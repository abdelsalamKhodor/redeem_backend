# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0016_store_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='redeem_process',
            field=models.CharField(blank=True, max_length=8, null=True, choices=[(b'barcode', b'Bar code'), (b'qrcode', b'QR code')]),
        ),
    ]
