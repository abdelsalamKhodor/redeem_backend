# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=50, verbose_name=b'Name')),
                ('mcc', models.CharField(default=b'000', max_length=8, verbose_name=b'MCC')),
            ],
        ),
        migrations.RemoveField(
            model_name='user',
            name='date_of_birth',
        ),
        migrations.RemoveField(
            model_name='user',
            name='name',
        ),
        migrations.RemoveField(
            model_name='user',
            name='username',
        ),
        migrations.AddField(
            model_name='announcer',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='announcer',
            name='logo_url',
            field=models.ImageField(null=True, upload_to=b'photos/deals', blank=True),
        ),
        migrations.AddField(
            model_name='announcer',
            name='phone',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='announcer',
            name='user_id',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='client',
            name='user_id',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='firstname',
            field=models.CharField(max_length=64, null=True, verbose_name=b'First name', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='gender',
            field=models.CharField(default=b'M', max_length=2, null=True, blank=True, choices=[(b'M', b'Male'), (b'F', b'Female')]),
        ),
        migrations.AddField(
            model_name='user',
            name='language',
            field=models.CharField(blank=True, max_length=2, null=True, choices=[(b'EN', b'English'), (b'AR', b'Arabic'), (b'FR', b'French')]),
        ),
        migrations.AddField(
            model_name='user',
            name='lastname',
            field=models.CharField(max_length=64, null=True, verbose_name=b'Last name', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='registration_token',
            field=models.CharField(max_length=64, null=True, verbose_name=b'Token', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='year_of_birth',
            field=models.IntegerField(null=True, verbose_name=b'Date of birth', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name=b'email', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='password',
            field=models.CharField(max_length=64, null=True, verbose_name=b'password', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.CharField(max_length=32, null=True, verbose_name=b'Phone', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='country',
            field=models.ForeignKey(verbose_name=b'Country', blank=True, to='redeem.Country', null=True),
        ),
    ]
