# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from redeem import settings


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0017_store_redeem_process'),
    ]
    
    db_name = settings.DB_NAME
    query = """
                INSERT INTO `%s`.`auth_group` (`id`,`name`) VALUES (1,'announcer') ON DUPLICATE KEY UPDATE `name` = `name`;
                INSERT INTO `%s`.`auth_group` (`id`,`name`) VALUES (2,'client') ON DUPLICATE KEY UPDATE `name` = `name`;
                INSERT INTO `%s`.`auth_group` (`id`,`name`) VALUES (3,'store') ON DUPLICATE KEY UPDATE `name` = `name`;
                """ % (db_name, db_name, db_name)

    operations = [
                  migrations.RunSQL(query) 
    ]
