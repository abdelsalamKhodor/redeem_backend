# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0013_auto_20160906_1435'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=50, verbose_name=b'Name')),
                ('country', models.ForeignKey(verbose_name=b'Country', blank=True, to='redeem.Country', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('phone', models.CharField(max_length=256, null=True, blank=True)),
                ('longitude', models.DecimalField(null=True, verbose_name=b'Longitude', max_digits=11, decimal_places=8, blank=True)),
                ('latitude', models.DecimalField(null=True, verbose_name=b'Latitude', max_digits=10, decimal_places=8, blank=True)),
                ('city', models.ForeignKey(verbose_name=b'City', blank=True, to='redeem.City', null=True)),
                ('client', models.ForeignKey(verbose_name=b'Client', blank=True, to='redeem.Client', null=True)),
                ('user', models.OneToOneField(null=True, blank=True, to='redeem.User')),
            ],
        ),
        migrations.CreateModel(
            name='Wifi',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ssid', models.CharField(max_length=64, verbose_name=b'SSID')),
                ('mac', models.CharField(max_length=64, verbose_name=b'MAC')),
                ('store', models.ForeignKey(verbose_name=b'Store', blank=True, to='redeem.Store', null=True)),
            ],
        ),
    ]
