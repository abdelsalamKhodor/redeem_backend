# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0009_auto_20160902_1124'),
    ]

    operations = [
        migrations.RenameField(
            model_name='deal',
            old_name='client_id',
            new_name='client',
        ),
    ]
