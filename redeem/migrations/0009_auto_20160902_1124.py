# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('redeem', '0008_auto_20160902_1022'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='user_id',
        ),
        migrations.AddField(
            model_name='client',
            name='user',
            field=models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
