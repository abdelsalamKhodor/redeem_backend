# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0011_auto_20160905_1341'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='notification_token',
            field=models.CharField(max_length=64, null=True, verbose_name=b'Notification Token', blank=True),
        ),
    ]
