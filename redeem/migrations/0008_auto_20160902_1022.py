# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redeem', '0007_auto_20160830_1320'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='physical',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='client',
            name='salesmen',
            field=models.BooleanField(default=False),
        ),
    ]
