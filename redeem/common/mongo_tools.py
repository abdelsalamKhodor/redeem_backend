'''
Created on Mar 11, 2016

@author: User
'''
from pymongo.mongo_client import MongoClient
import logging
from redeem import settings

log = logging.getLogger(__name__)

def get_db():
    """
    Returns a DB instance for cardreader.
    @note make sure to close the connection afterwards via mongo_tools.close(db)
    """
    return MongoClient(settings.MONGO_HOST, settings.MONGO_PORT).redeem

def close(db):
    try:
        db.client.close()
    except:
        log.debug('Failed to close mongo db connection')