# -*- coding: UTF-8 -*-
'''
Created on May 22, 2014

@author: Rabih Kodeih
'''
from datetime import timedelta
import datetime
from functools import wraps
import inspect
import json
import logging
import math
import re
import types
import random

from django.template.context_processors import csrf
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.http.response import HttpResponse
from django.http.response import HttpResponseForbidden
from django.shortcuts import render_to_response
from django.template.context import RequestContext
import requests

import unicodedata
from django.utils import timezone
import time
from django import db
import hashlib

log = logging.getLogger(__name__)
time_log = logging.getLogger('time_logger')

ti_session = None

#===============================================================================
# URL Patterns
#===============================================================================

def _pat(url=None, *params):
    """ convinience function to be used in urls.py
    """
    return r'^%s%s%s$' % (url, u''.join('(?:/(?P<%s>[A-Za-z0-9_\-]*))?' % p for p in params), u'/' if url else u'')


#===============================================================================
# GET View Helpers
#===============================================================================

def renders_template(template):
    """ Decorator for template rendering
    """
    def decorator(func):
        @wraps(func)
        def innerClosure(*args, **kws):
            res = func(*args, **kws)
            request = args[0]
            #if res.__class__ == dict:
            if u'request' in res:
                return render_to_response(template, res, RequestContext(request))
            return res
        return innerClosure
    return decorator


#===============================================================================
# POST View Helpers
#===============================================================================

def is_post_handler(func):
    """ Decorator for asserting that a view is a post handler
    """
    @wraps(func)
    def closure(request, *args, **kwargs):
        if request.method not in ['POST', 'OPTIONS']:
            return HttpResponseForbidden()
        if request.method == 'OPTIONS': return ajax_response({})
        return func(request, *args, **kwargs)
    return closure

def ajax_response(data):
    """ Formats json data as a proper ajax response
    """
    response = HttpResponse(json.dumps(data, ensure_ascii=False, encoding='utf-8', default=lambda o: o.isoformat() if hasattr(o, 'isoformat') else o), content_type='application/json')
    return response

def is_number(value):
    """ checks wheter value is a number (ex: float or int)
    """
    try:
        value + 1
        return True
    except TypeError:
        return False

def _dob(value):
    """ Convenience function to get datetine object from string (ex: January/01/1990)
    """
    if not value: return None
    try:
        t = datetime.datetime.strptime(value ,"%Y-%m-%d")
    except:
        raise ValidationError('<strong>Date of Birth</strong>Invalid format, please provide a valid value (yyyy-mm-dd)')
    return t
    
def _v(post_val):
    """ convenience function to get a value from a request post object that handles nullable and zero values
    """
    try:
        if int(float(post_val)) == 0: return 0
    except:
        pass
    return post_val if post_val else None


#===============================================================================
# DB Query Pagination
#===============================================================================

def paginate_query_set(query_set, page_num, items_per_page):
    tmp = []
    tot = len(query_set)
    for item in query_set[page_num*items_per_page: (page_num + 1)*items_per_page]:
        row = {}
        row['type'] = 'data'
        row['model'] = item
        tmp.append(row)
    if page_num*items_per_page + len(tmp) < tot and len(tmp) > 0:
        tmp.append({'type': 'more', 'page_num': page_num + 1})
    elif page_num*items_per_page + len(tmp) == tot and page_num > 0:
        tmp.append({'type': 'more', 'page_num': page_num + 1, 'last_page': True})
    return tmp

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


#===============================================================================
# Database String Search
#===============================================================================

def _normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 


def get_search_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.
        
        @param query_string: space separated tokens
        @param search_field: list of fields or list of tuples(field name, operator_name) in the model object to search. 
            If a list specified then all fields are filtered by <field_name>__icontains
            if a list of tuple, each tuple contains the field in first position and the operator in the second  
        Usage Example:
            entry_query = get_query(query_string, ['title', 'body',])
            Model.objects.filter(entry_query).order_by('-pub_date')
            
            entry_query = get_query(query_string, [('title', 'startswith'), ('id', 'exact')])
    
    '''
    query = None # Query to search for every search term        
    # terms = _normalize_query(query_string) #note: this shouldn't be used as we have exact and startswith queries
    terms = [query_string.strip()]
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            if type(field_name) == types.TupleType:
                q = Q(**{"%s__%s" % field_name: term})
            else:
                q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query


#==============================================================================
# Titanium Push Notifications
#==============================================================================

def ti_get_session_id(app_key, login, password, fresh=False):
    global ti_session
    
    if fresh or not ti_session:
        ti_session = ti_authenticate(app_key, login, password)

    return ti_session


def ti_authenticate(app_key, login, password):
    url = u"https://api.cloud.appcelerator.com/v1/users/login.json?key=%s&pretty_json=true" % app_key
    data = {'login': login, 'password':password}
    
    log.debug("Calling %s with data: %s", url, data)
    r = requests.post(url, data)
    resp = r.json()
    code = resp['meta']['code']
    if code != 200:
        log.error('Ti authentication failure: %s' % unicode(resp['meta']['message']))
        session_id = None
    else:
        session_id = resp['meta']['session_id']
    return session_id

def log_post(request, result):
    try:
        frm = inspect.stack()[1]
        mod = inspect.getmodule(frm[0])
        originator = mod.__name__
        
        log_data = {}
        for k, v in request.POST.iteritems():
            if k in ['token', 'session_token', 'device_token', 'password', 'pwd']: continue
            log_data[k] = v
        log_result = {}
        for k, v in result.iteritems():
            log_result[k] = v if type(v) != list else (u"list of %s item(s)" % len(v))
        log.debug("from '%s', result: %s, request: %s" % (originator, log_result, log_data))
    except:
        log.error("Failed to log_post, nothing harmful but good to fix", exc_info=1)

    
def _print(message=None):
    print
    print '=' * 70
    print;print;print;
    print message 
    print;print;print;
    print '=' * 70
    print


def toAutoCompleteMap(uid, label, value):
    return {'id': uid, 'label': label, 'value': value}

def isNumber(nbStr):
    isNum = False
    
    try:
        float(nbStr)
        isNum = True
    except:
        pass
            
    return isNum

def toFloat(nbr):
    '''
     Check if the nubmer is float then it returns it as is. if not then it checks if number and returns it
     if not then it checks if the input is empty then returns zero otherwise will return the input unchanged
    '''
    if isinstance(nbr, types.FloatType):
        return nbr
    elif isNumber(nbr):
        return float(nbr)
    else:
        return nbr if str(nbr).strip() != '' else float(0);
    
def toInt(nbr):
    '''
     Check if the nubmer is int then it returns it as is. if not then it checks if number and returns it
     if not then it checks if the input is empty then returns zero otherwise will return the input unchanged
    '''
    if isinstance(nbr, types.IntType):
        return nbr
    elif isNumber(nbr):
        return int(float(nbr))
    else:
        return nbr if str(nbr).strip() != '' else int(0);
    
    
def defaultContextData(request, ctx=None):
    """"
     Inject in a context the default attributes used in most pages. 
     ctx['user'] if authenticated
     csrf initialization
     ctx['APP_NAME'] in order to be changed on the file without the need to hard code it 
    """
    
    if not ctx:
        ctx = {}
    
    if request.user is not None and request.user.is_authenticated():
        ctx['user'] = request.user
    
    ctx.update(csrf(request))
    
    if request.GET:
        ctx.update(request.GET)
            
    if request.POST:
        ctx.update(request.POST)
    
    ctx['APP_NAME'] = 'Bots Management'
    
    return ctx;
    
def populateContextForGrid(ctx, rs, rows, page, sidx, sord):
    """
     Fils the necessary data for the grid in order to be used in the XML data of the JQGrid .
     @param rs: the result set returned from the execution of ModelClass.objects.all() or ModelClass.objects.filter()
     @param rows: number of rowas per page
     @param page: page number
     @sidx: sort index
     @sord: sort order "asc" or "desc".
     
     Note: all those parameters: rows, page, sidx, sord are returned from the JQGrid 
    """
    
    rows = int(rows)
    page = int(page)
    
    totalRecords = rs.count()
    
    sortOn = "-" + sidx if sord == "desc" else sidx
    
    rs = rs.order_by(sortOn)[(page - 1) * rows: page * rows]
    
    ctx['grid'] = list(rs)
    ctx['page'] = page
    ctx['totalRecords'] = totalRecords
    ctx['totalPages'] = math.ceil(float(totalRecords) / rows)
    ctx['rows'] = rows

    
    return ctx;

def nullToEmpty(strIn, defaultStr = ""):
    """
     Same as the Apache common StringUtil.defaultString
     if null or empty it returns the defaultStr
    """
    if strIn == None or strIn.strip() == "":
        return defaultStr
    else:
        return strIn
    
    
    

def get_previous_byday(dayName):
    """
    get the date of specific day in the current week
    @param dayName: name of the desired name
    @return: date object 
    """
    weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday','Friday', 'Saturday', 'Sunday']
    #get date of today
    start_date = timezone.now()
    #get number of today in the week (Monday : 0, Tuesday:1 ...)
    day_num = start_date.weekday()
    #get the index of target day from the above list
    day_num_target = weekdays.index(dayName)
    #get number of days between today and the selected day
    days_ago = (7 + day_num - day_num_target) % 7
#     if days_ago == 0:
#         days_ago = 7

    #get the date of targeted day
    target_date = start_date - timedelta(days=days_ago)
    #set hours minutes and seconds to 00 for the target date
    date_obj =  target_date.replace(hour = 00, minute = 00, second = 00).strftime('%Y-%m-%d %H:%M:%S')
   
    return date_obj

def get_first_of_this_month():
    """
    get the date of first day in the month , 
    example : if today is 20/5/2015 then the target date is 1/5/2015
    this will bed done with replacing the date of today by 1 and set the time to 00:00:00
    """
    today = timezone.now()
    date_obj =  today.replace(day = 1, hour = 00, minute = 00, second = 00)
    return date_obj


def get_first_of_last_three_month():
    """
    get the date of the first day in the pre-previous month,
    example : if today is : 20/5/2015 then the target date is 1/3/2015 
    """
    #get date of today
    today = timezone.now()
    #get index number of current month
    this_month = int(today.strftime("%m"))
    #get number of second prebious month
    target_month=this_month-2
    target_year = int(today.strftime("%Y"))
    if(target_month <= 0):
        target_month = 12 + target_month
        target_year = target_year - 1
    #set time to 00:00:00 and day to 1    
    date_obj =  today.replace(year = target_year, month = target_month,day = 1, hour = 00, minute = 00, second = 00)
    return date_obj

def get_first_of_next_month():
    today = timezone.now()
    this_month = int(today.strftime("%m"))
    this_year = int(today.strftime("%Y"))
    target_month = this_month + 1
    target_year = this_year
    if target_month > 12:
        target_month = 1
        target_year = this_year + 1
    result_date = today.replace(year = target_year, month = target_month,day = 1, hour = 00, minute = 00, second = 00)
    return result_date

def get_dates_for_this_week():
    #get date for every day in this week 
    monday = datetime.datetime.strptime(get_previous_byday("Monday"), '%Y-%m-%d %H:%M:%S')
    tuesday = monday + datetime.timedelta(days=1)
    wednesday = monday + datetime.timedelta(days=2)
    thursday = monday + datetime.timedelta(days=3)
    friday = monday + datetime.timedelta(days=4)
    saturday = monday + datetime.timedelta(days=5)
    sunday = monday + datetime.timedelta(days=6)
    
    days = {}
    days['Monday'] = monday
    days['Tuesday'] = tuesday
    days['Wednesday'] = wednesday
    days['Thursday'] = thursday
    days['Friday'] = friday
    days['Saturday'] = saturday
    days['Sunday'] = sunday
    
    return days

def getWeeksOfLastThreeMonth():
    # get weeks dates in last three months
    week1 = get_first_of_last_three_month()
    week2 = week1 + datetime.timedelta(days = 7)
    week3 = week1 + datetime.timedelta(days = 14)
    week4 = week1 + datetime.timedelta(days = 21)
    week5 = week1 + datetime.timedelta(days = 28)
    week6 = week1 + datetime.timedelta(days = 35)
    week7 = week1 + datetime.timedelta(days = 42)
    week8 = week1 + datetime.timedelta(days = 49)
    week9 = week1 + datetime.timedelta(days = 56)
    week10 = week1 + datetime.timedelta(days = 63)
    week11 = week1 + datetime.timedelta(days = 70)
    week12 = week1 + datetime.timedelta(days = 77)
    week13 = week1 + datetime.timedelta(days = 84)
    
    result = {'Week1':week1, 'Week2':week2, 'Week3':week3, 'Week4':week4, 'Week5':week5, 'Week6':week6, 'Week7':week7, 'Week8':week8, 'Week9':week9, 'Week10':week10, 'Week11':week11,'Week12': week12, 'Week13':week13}
    return result

def get_all_mondays_dates_in_month():
    today = timezone.now()
    # Get weeks dates in current month
    week1 = today.replace(day = 1,hour = 00, minute = 00, second = 00)
    week2 = week1 + datetime.timedelta(days = 7)
    week3 = week1 + datetime.timedelta(days = 14)
    week4 = week1 + datetime.timedelta(days = 21)
    week5 = week1 + datetime.timedelta(days = 28)
    
    result = {'Week1':week1, 'Week2':week2, 'Week3':week3, 'Week4':week4, 'Week5':week5}
    
    return result

def get_formatted_duration(duration):
    """
    this function return a formatted time from given number of seconds
    @param duration: duration by second
    @return : string as '03h 45min 50s' 
    """
    hours = int(duration/3600)
    nbHourPerSeconds = hours*3600
    remainingSec = duration-nbHourPerSeconds
    mins = int(remainingSec/60)
    nbMinPerSec = mins*60
    remainingSec = int(remainingSec - nbMinPerSec)
    formattedDuration = str(hours)+"h " + str(mins) + "m " + str(remainingSec)+"s"
    return formattedDuration

def get_converted_data_size(num):
    """
    convert data consumption to a readable unit
    123456789 KB ==> 123.46 GB
    """
    suffix = 'B'
    res = ''
    for unit in ['K','M','G','T','P','E','Z','Y']:
        if abs(num) < 1024.0:
            res =  "%3.1f%s%s" % (num, unit, suffix)
            break
        num /= 1024.0
    return res
def get_random_color():
    r = lambda: random.randint(0,255)
    color = ('#%02X%02X%02X' % (r(),r(),r()))
    return color

def date_conv(unicode_arabic_date):
    new_date = ''
    for d in unicode_arabic_date:
        if d != '-' and d != ':' and d != ' ':
            new_date+=str(unicodedata.decimal(d))
        else:
            new_date += str(d)
    return new_date
def fix_timeStamp(timeStamp):
    res_timeStamp = timeStamp.encode('utf-8')
    res_timeStamp = unicode(res_timeStamp, 'utf-8')
    res_timeStamp = date_conv(res_timeStamp)
    timeStamp = datetime.datetime.strptime(res_timeStamp, '%Y-%m-%d %H:%M:%S')
    return timeStamp

def get_week_of_month(date):
    dom = date.day
    week = 5
    if dom <= 7:
        week = 1
    elif dom > 7 and dom <= 14:
        week = 2
    elif dom > 14 and dom <= 21:
        week = 3
    elif dom > 21 and dom <= 28:
        week = 4
    return week       

def log_time(func):
    """
     Decotrator that logs the execution time of the given function as log.info
     
    """
    
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        before = time.time()
        
        fn_result = func(*args, **kwargs)
        
        time_log.info("Elapsed time to execute %s is %0.2fs", func.__name__, time.time() - before)
        
        return fn_result
    
    return func_wrapper
        

def log_time_queries(func):
    """
     Decorator that logs the time and total queries for the given function
    """
    
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        before = time.time()
        queriesBefore = len(db.connection.queries)
        
        fn_result = func(*args, **kwargs)
        
        log.info("Elapsed time to execute %s is %0.2fs using %s queries", func.__name__, time.time() - before, len(db.connection.queries) - queriesBefore)
        
        return fn_result

def get_first_of_prev_month():
    first_of_this_month = get_first_of_this_month()
    month = first_of_this_month.month
    prev_month = month-1
    if prev_month == 0 :
        prev_month = 12
    first_of_prev_month = first_of_this_month.replace(month=prev_month) 
    return first_of_prev_month    

def get_hash_of_string(value):
    hashed_obj = hashlib.md5(value)
    hashed_value = hashed_obj.hexdigest()
    return hashed_value
    
    