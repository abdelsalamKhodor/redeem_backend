'''
Created on Aug 31, 2016

@author: User
'''
class group():
    ANNOUNCER="announcer"
    CLIENT="client"
    STORE="store"


ENGLISH = 'EN'
ARABIC = 'AR'
FRENCH = 'FR'
LANGUAGE_CHOICES = (
    (ENGLISH, 'English'),
    (ARABIC, 'Arabic'),
    (FRENCH, 'French'),
    )


BARCODE = 'barcode'
QRCODE = 'qrcode'
REDEEM_PROCESS_CHOICES = (
    (BARCODE, 'Bar code'),
    (QRCODE, 'QR code'),
)
    