'''
Created on Aug 22, 2016

@author: User
'''
from redeem.views.api import get_categories, get_sub_categories, register_user,\
    login_user, edit_user_cat, get_user_cat
from django.conf.urls import url
from redeem.views.notification_api import receive_notification_token,\
    receive_notification_status, get_notifications_list_by_user,\
    get_notification_details
urls = [
        url(r'^register_user$', register_user , name="register_user"),
        url(r'^login_user$', login_user , name="login_user"),
        url(r'^get_categories$', get_categories , name="get_categories"),
        url(r'^get_sub_categories$', get_sub_categories , name="get_sub_categories"),
        url(r'^edit_user_cat$', edit_user_cat , name="edit_user_cat"),
        url(r'^get_user_cat$', get_user_cat , name="get_user_cat"),
        url(r'^receive_notification_token$', receive_notification_token , name="receive_notification_token"),
        url(r'^receive_notification_status$', receive_notification_status , name="receive_notification_status"),
        url(r'^get_notifications_list_by_user$', get_notifications_list_by_user , name="get_notifications_list_by_user"),
        url(r'^get_notification_details$', get_notification_details , name="get_notification_details"),

        ]